Ecosynther Roadmap

	* Ecosynther v0.7
		- release date: Sep 30, 2013
		- components: 1) GPU feature extraction; 2) full set feature matching; 3) CPU bundle adjustment for sparse point cloud reconstruction; 4) CPU densification operation for dense point cloud generation;

	* Ecosynther v0.8
		- release date: Feb 26, 2014
		- updates: add 1 pre-processing module to filter image sets based on their GPS coordinates to reduce the image matching compute time;

	* Ecosynther v1.0
		- release date: TBD
		- updates: turn off debug output
