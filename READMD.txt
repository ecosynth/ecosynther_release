# AWS Ecosynther -- 2015-03-17 --

Instructions for cloning available here: https://drive.google.com/open?id=1gUTo4d_b_JbCXzdvF2zizwd8Bki37Q3fCEIiO6sryIc&authuser=0

# Ecosynther v0.7 -- 2013-09-30 --

Latest release version: Ecosynther_v0.7_PMVS_auto.zip

# Ecosynther v0.8 -- 2014-02-26 --

Latest release version: Ecosynther_v0.8.zip